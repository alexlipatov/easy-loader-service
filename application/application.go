package application

import (
	"easy_loader/auth"
	"easy_loader/lib"
	"easy_loader/loader"
	"easy_loader/repository"
	tg "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	"github.com/pkg/errors"
	log "github.com/sirupsen/logrus"
	"strings"
)

// NewApplication builds application from its dependencies
func NewApplication(
	cfg lib.Config,
	client *tg.BotAPI,
	loader loader.ILoader,
	authorizer auth.IAuthorizer,
	repo repository.IRepository,
) *Application {
	return &Application{
		config:     cfg,
		client:     client,
		loader:     loader,
		authorizer: authorizer,
		repository: repo,
	}
}

// Application is a core struct, that handles user messages with Photos and Docs
type Application struct {
	config     lib.Config
	client     *tg.BotAPI
	loader     loader.ILoader
	authorizer auth.IAuthorizer
	repository repository.IRepository
}

// Run starts listening for updates and handles them
func (app Application) Run() {
	updates := app.getUpdates()

	for update := range updates {
		err := app.handleUpdate(update)
		if err != nil {
			log.WithError(err).Errorf("failed to handle update properly")
		}
	}
}

func (app Application) getUpdates() tg.UpdatesChannel {
	updateConfig := tg.NewUpdate(0)
	updateConfig.Timeout = 30
	updates := app.client.GetUpdatesChan(updateConfig)
	return updates
}

func (app Application) handleUpdate(update tg.Update) error {
	if update.Message == nil {
		return nil
	}

	if update.Message.From == nil {
		log.Warning("got message sent to channel, that application don't handle")
		return errors.New("unable to get sender")
	}

	userName := update.Message.From.UserName
	isAuthorized := app.authorizer.Auth(userName)

	if !isAuthorized {
		log.Debugf("unauthorized user: %v tried to use application", userName)
		err := app.reply(update, "unauthorized")
		return err
	}

	// Debugging update content
	// log.Debugf("Update message content: %+v", update.Message)

	fileId, fileType, err := Extract(update)
	if err != nil {
		log.WithError(err).Debug("update doesn't contain any available documents")
		err = app.reply(update, "files not found")
		return err
	}

	if app.allowedDocumentType(fileType) {
		return app.handle(update, fileId)
	}

	// TODO: provide list of available file types
	log.Debugf("type: %s is not allowed", fileType)
	err = app.reply(update, "file type is not allowed.")
	return nil
}

func (app Application) allowedDocumentType(mimeType string) bool {
	for _, prefix := range app.config.AllowedTypes.WithPrefix {
		if strings.HasPrefix(mimeType, prefix) {
			return true
		}
	}
	for _, t := range app.config.AllowedTypes.Specific {
		if t == mimeType {
			return true
		}
	}
	return false
}

func (app Application) handle(update tg.Update, fileId string) (err error) {
	// Reply to sender about results
	defer func() {
		if err != nil {
			log.WithError(err).Debug("failed to handle update properly")
			err = app.reply(update, "Failed")
		} else {
			err = app.reply(update, "Ok")
		}
	}()

	filePath, err := app.client.GetFileDirectURL(fileId)
	if err != nil {
		return errors.Wrapf(err, "unable to get file url")
	}

	file, fileName, err := app.loader.Load(filePath)
	if err != nil {
		return errors.Wrap(err, "unable to load file by url")
	}

	_, err = app.repository.Store(file, fileName)
	if err != nil {
		return errors.Wrapf(err, "unable to store download file")
	}

	return nil
}

func (app Application) reply(update tg.Update, msg string) error {
	messageConfig := tg.NewMessage(update.Message.From.ID, msg)
	messageConfig.ReplyToMessageID = update.Message.MessageID
	_, err := app.client.Send(messageConfig)
	return err
}
