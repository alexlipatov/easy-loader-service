package application

import (
	tg "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	"github.com/pkg/errors"
)

// Extract извлекает необходимые данные из update структуры
func Extract(update tg.Update) (fileId string, typeName string, err error) {
	if update.Message == nil {
		return "", "", errors.New("update doesn't contain message struct")
	}

	switch {
	case update.Message.Photo != nil:
		return update.Message.Photo[len(update.Message.Photo)-1].FileID, "image", nil
	case update.Message.Document != nil:
		return update.Message.Document.FileID, update.Message.Document.MimeType, nil
	default:
		return "", "", errors.New("nothing to extract from message")
	}
}
