package loader

import (
	"github.com/pkg/errors"
	"io"
	"net/http"
	"strings"
)

type ILoader interface {
	Load(fileUrl string) (io.Reader, string, error)
}

func NewLoader() ILoader {
	return &loader{}
}

type loader struct {
}

func (l loader) Load(fileUrl string) (io.Reader, string, error) {
	response, err := http.Get(fileUrl)
	if err != nil {
		return nil, "", errors.Wrap(err, "unable to load file")
	}
	if response.StatusCode != http.StatusOK {
		return nil, "", errors.New("unable to load file, bad status")
	}

	//Get full filename from url
	fileName := getFilenameFromUrl(fileUrl)

	return response.Body, fileName, nil
}

func getFilenameFromUrl(url string) string {
	urlParts := strings.Split(url, "/")
	return urlParts[len(urlParts)-1]
}
