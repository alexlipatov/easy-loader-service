package lib

import (
	log "github.com/sirupsen/logrus"
)

// SetUpLogger configures logger
func SetUpLogger() {
	log.SetFormatter(&log.JSONFormatter{
		FieldMap: log.FieldMap{
			log.FieldKeyTime:  "time",
			log.FieldKeyLevel: "severity",
			log.FieldKeyMsg:   "message",
		},
	})

	log.SetLevel(log.DebugLevel)
}
