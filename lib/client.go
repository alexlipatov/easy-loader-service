package lib

import (
	tg "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	log "github.com/sirupsen/logrus"
)

// NewClient builds new telegram bot api client from configuration
func NewClient(cfg Config) *tg.BotAPI {
	client, err := tg.NewBotAPI(cfg.Telegram.Token)
	if err != nil {
		log.WithError(err).Fatalf("failed to connect to telegram api")
	}
	return client
}
