package lib

import (
	"github.com/spf13/viper"
	"strings"
)

type Config struct {
	AllowedTypes struct {
		WithPrefix []string
		Specific   []string
	}
	LocalRepository struct {
		DirPath string
	}
	Telegram struct {
		Token string
		User  string
	}
}

// MustLoad provides config or panic
func MustLoad() Config {
	config, err := getConfig()
	if err != nil {
		panic(err)
	}
	return config
}

// getConfig returns loaded config struct
func getConfig() (Config, error) {
	var config Config
	err := load(&config)
	return config, err
}

func load(config interface{}) error {
	viper.AddConfigPath(".")
	viper.SetConfigName("Config")
	viper.SetConfigType("yaml")
	viper.AutomaticEnv()
	viper.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))

	err := viper.ReadInConfig()
	if err != nil {
		return err
	}
	err = viper.Unmarshal(config)
	return err
}
