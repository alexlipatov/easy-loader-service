package bootstrap

import (
	"context"
	"easy_loader/application"
	"easy_loader/auth"
	"easy_loader/lib"
	"easy_loader/loader"
	"easy_loader/repository"
	log "github.com/sirupsen/logrus"
	"go.uber.org/fx"
)

var Module = fx.Options(
	lib.Module,
	auth.Module,
	loader.Module,
	repository.Module,
	application.Module,
	fx.Invoke(bootstrap),
)

func bootstrap(
	lifecycle fx.Lifecycle,
	app *application.Application,
) {
	lifecycle.Append(fx.Hook{
		OnStart: func(_ context.Context) error {
			log.Info("Starting application")
			go func() {
				app.Run()
			}()
			return nil
		},
		OnStop: func(_ context.Context) error {
			log.Printf("Stopping application")
			return nil
		},
	})
}
