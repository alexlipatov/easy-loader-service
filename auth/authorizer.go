package auth

import "easy_loader/lib"

type IAuthorizer interface {
	Auth(username string) bool
}

func NewAuthorizer(cfg lib.Config) IAuthorizer {
	return &singleUserAuthorizer{user: cfg.Telegram.User}
}

type singleUserAuthorizer struct {
	user string
}

func (s singleUserAuthorizer) Auth(username string) bool {
	return s.user == username
}
