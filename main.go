package main

import (
	"easy_loader/bootstrap"
	"easy_loader/lib"
	"go.uber.org/fx"
)

func main() {
	lib.SetUpLogger()
	fx.New(bootstrap.Module).Run()
}
