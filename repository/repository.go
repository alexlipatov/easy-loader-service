package repository

import (
	"easy_loader/lib"
	"github.com/pkg/errors"
	log "github.com/sirupsen/logrus"
	"io"
	"os"
	"path/filepath"
	"time"
)

type IRepository interface {
	Store(reader io.Reader, fileName string) (string, error)
}

func NewRepository(cfg lib.Config) IRepository {
	p := cfg.LocalRepository.DirPath
	absPath, err := filepath.Abs(p)

	err = os.MkdirAll(absPath, 700)
	if err != nil {
		log.WithError(err).Fatal("unable to create storage folder")
	}

	if err != nil {
		log.WithError(err).Fatal("unable to define abs path for local repository")
		panic(err)
	}

	info, err := os.Stat(absPath)
	if err != nil {
		log.WithError(err).Fatal("unable to get info about provided directory")
		panic(err)
	}
	if !info.IsDir() {
		log.WithError(err).Fatal("provided path is not a directory")
	}

	// TODO: check write permissions on init local repository
	return localRepository{dirPath: absPath}
}

type localRepository struct {
	dirPath string
}

func (repo localRepository) Store(file io.Reader, fileName string) (string, error) {
	folderPath, err := repo.getCurrentFolder()

	filePath := filepath.Join(folderPath, fileName)
	newFile, err := os.Create(filePath)
	if err != nil {
		return "", errors.Wrap(err, "unable to create new file")
	}

	_, err = io.Copy(newFile, file)
	if err != nil {
		return "", errors.Wrapf(err, "unable to save file")
	}

	err = newFile.Close()
	if err != nil {
		return "", errors.Wrap(err, "unable to close saved file")
	}

	return filePath, nil
}

func (repo localRepository) getCurrentFolder() (string, error) {
	currentFolderName := time.Now().Format("2006-01-02")
	folderPath := filepath.Join(repo.dirPath, currentFolderName)
	err := os.Mkdir(folderPath, 700)
	return folderPath, err
}
